title: Zoopla
file_name: zoopla
canonical_path: /customers/zoopla/
cover_image: /images/blogimages/zoopla_cover_image.jpg
cover_title: |
  How Zoopla deploys 700% faster with GitLab
cover_description: |
  Zoopla, the UK’s most comprehensive property destination, adopted GitLab Self Managed Premium for version control, CI/CD, and improved developer collaboration.
twitter_image: /images/blogimages/zoopla_cover_image.jpg
twitter_text: Learn how @zoopla deploys 700% faster with GitLab
customer_logo: /images/case_study_logos/zoopla_logo_brightpurple_rgb.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: United Kingdom
customer_solution: GitLab Self Managed Premium
customer_employees: |
  700
customer_overview: |
  Zoopla, the UK’s most comprehensive property destination, adopted GitLab for accelerated deployment and a unified workflow.
customer_challenge: |
  Zoopla was looking for a robust platform that could simplify their existing multi-toolchain environment and transform their infrastructure.
key_benefits: >-

  
    AWS integration 

  
    Jira integration 

  
    End-to-end visibility

  
    Easy user interface 

  
    Accelerated deployment
customer_stats:
  - stat: 2 hours
    label: Lead time reduced to 2 hours down from 5 days 
  - stat: Daily deploys 
    label: From weekly to daily deployments 
  - stat: 0%
    label: Change failure rate at 0% down from 40%
customer_study_content:
  - title: the customer
    subtitle: Leading real estate portal
    content: >-
    
  
       Zoopla is the United Kingdom’s most comprehensive property destination. [Zoopla](https://www.zoopla.co.uk/) provides millions of users with access to properties for sale and for rent, along with detailed valuation information on nearly 29 million UK homes, in one place.The website enables estate agencies and developers to promote properties they want to sell. On top of that, Zoopla uses detailed valuation software to allow for valuation of properties to be done automatically along with connecting homeowners with local agents if they want a more detailed valuation.

  - title: the challenge
    subtitle:  Managing too many tools without visibility
    content: >-
    

        Zoopla developers were managing a number of tools for source control and running pipelines, primarily Jenkins and Apache Groovy. Between the lack of plugin support for Jenkins and the complexity of Groovy, the team was looking for a way to simplify the existing workflow. “Maintaining Jenkins, understanding Groovy, and creating complex builds that you can't really easily package with the code, was a bit of a challenge,” according to Gustav Fit, Engineering Lead.

        Zoopla was looking for an all-in-one solution  in order to facilitate development management. The development team wanted “A tool that offers multiple points, you can store the source control, you can simply build a pipeline, it gives you a nice visual support and gives you reports, and then you can even do schedules. And you can do it without much thinking,” Fit said.


  - title: the solution
    subtitle:  SCM, CI, and CD in one platform
    content: >-

        According to Fit, Zoopla adopted GitLab Premium as a “strategic and primary tool” at the start of a big transformation project. The notable added value in their adoption decision was regarding DORA metrics and GitLab’s advanced API capabilities. “We tried using other build pipeline provider API’s, but they still have a very long way to go to get to a place where it's easy to use ...” Fit said.
      
        Another benefit of adopting GitLab is that now the team uses a single platform for continuous integration, deployment, and source control management. The robust workflow efficiency is the ideal solution they were searching for.
      
        On top of that, GitLab is becoming a market standard for engineers and developers. “[Engineers] basically come with a specific skill set, they will be expecting a certain set of tools. And [GitLab] is becoming a market standard, because it is very usable and user friendly,” Fit added.
       
        
  - title: the results
    subtitle:   Improved APIs, faster deploys, AWS integration
    content: >-

        GitLab is now Zoopla’s primary tool for delivery, source control, APIs, and pipelines. The team also uses GitLab issues for the Request For Comments process; which would not have been adopted so rapidly, if not for GitLab’s features supporting this use case. “When we moved to GitLab, we moved from a weekly process that we couldn't properly measure, to a process that takes around 45 minutes now, but runs the same set of quality assurances in all of the deployments,” Fit said. “We still think about how we can make it smaller, and how we can measure it. So, that's absolutely wicked.”

    
        Zoopla uses deployment frequency, change failure rate, MTTR, and cycle time metrics. Since the transformation project, which included moving to GitLab and also using the platform that supports the project, teams have seen their metric numbers dramatically improve in all areas. “Without GitLab, we wouldn't be able to measure them. So this would be the key point, and that's also why I really like the way the API is designed. There's a lot more that we can be doing with just the API, because without that, we wouldn't be able to measure all of that stuff, because we wouldn't know,” Fit added. 

    
        Gitlab was one of the key elements that allowed teams to improve deployment frequency for the key public websites from once per week to now once daily. On top of that, lead time went down from “five business days to just under two hours,” according to Fit. The change fail rate is trending from 40% towards a flat 0% rate as of now. “If we're comparing our previous solutions, Jenkins, it takes me around 15 minutes just to create the deployment pipeline. I can't remember any other place, or any other tool where it is so simple and provides all of the additional benefits of GitLab. I'm a great fan of just storing the build configuration in code, as a configuration file,” Fit added.    

    
        Engineers have started using GitLab features for integrating tests into pipelines. Though this is a possibility in other platforms, the simplicity of creating a pipeline in GitLab and then adding dependencies on the pipeline is unique toGitLab. “It gives you a very nice user interface, where you can just basically go in and see, especially if you're a quality engineer. You can go in and see what failed in the console,” according to Fit. With GitLab, developers no longer need to click through multiple screens in order to view the data they need. This single process allows teams to measure DORA metrics and assess efficiencies. On top of that, the teams are moving towards a place where everything is standardised. Instead of having a tool that needs constant support and a lengthy learning curve, everyone knows how to use GitLab. Developers own and manage their own pipelines, without worrying too much about how it should work. 


        Developers are able to execute the relevant discrete parts of the pipeline on their own and with little supervision. “Whether it's rollback or standard deployment, I am really happy we have the GitLab pipelines” , Fit said. Zoopla started with AWS in 2011. Developers deploy GitLab to the cloud using AWS and have never had any issues with the integration. ”The AWS integration works very well and is reliable. The teams are doing a massive move from the old architecture that Zoopla was using when they were a startup. Now, as a more mature company, Zoopla is becoming a more distributed architecture with GitLab as the driving force behind the upgrades and scaling efficiencies. “  added Fit. 


       

        

    
  
        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: I personally absolutely love GitLab. And I would say multiple times, here internally, that I think your API is very sexy, in that respect, that it's really great to work with it. It's very intuitive.
    attribution: Gustav Fit
    attribution_title: Engineering Lead, Zoopla








