---
layout: handbook-page-toc
title: "Audit Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Audit Committee Composition

- **Chairperson:** Karen Blasing
- **Members:** Sunny Bedi, David Hornik
- **Management DRI:** Chief Financial Officer

## Audit Committee Charter

**THE AMENDED AND RESTATED CHARTER WAS APPROVED BY THE BOARD ON 2021-09-14**

Please click here to read the [Audit Committee Charter](https://ir.gitlab.com/static-files/f8d37e2d-d022-4194-996f-6d194cd02173)

### Audit Committee Agenda Planner

We review the below topics no less frequently than the following schedule:

#### Management, Accounting and Reporting

|#|Topics                                                              | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:|:-------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---:  |
| 1 |Accounting policies                                               |       |       |       |       |    X   |
| 2 |Significant estimates and judgements                              |       |       |       |       |    X   |
| 3 |New accounting standards – impact and implementation plan         |       |       |       |       |    X   |
| 4 |Review of financial Statements <br> (if applicable, GAAP and Non-GAAP financials/metrics) |   X   |    X   |   X   |   X |       |
| 5 |Treasury                                                          |       |   X   |   X   |   X   |       |
| 6 |Investment                                                        |       |   X   |       |       |       |
| 7 |Review of financial statement risk                                |       |   X   |       |       |       |
| 8 |Insurance coverage update                                         |       |       |   X   |       |       |
| 9 |Close process                                                     |       |    X  |       |       |       |
|10 |Stock transactions                                                |       |       |       |       |   X   |
|11 |Tax audits / Taxes                                                |       |       |       |       |   X   |
|12 |Guidance model (if applicable)                                    |       |       |       |       |   X   |
|13 |Audit Update                                                      |   X   |   X   |   X   |   X   |       |
|14 |Attrition                                                         |   X   |   X   |   X   |   X   |       |
|15 |Quarterly Calendar                                                |   X   |       |       |       |       |
|16 |Organization Overviews - Accounting, Finance, Tax, FP&A, Investor Relations       |       |   X   |       |       |       |

#### People Division

|#|Topics                                                                  | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Global staffing update, succession plan and continuous improvement   |       |   X   |       |       |      |
| 2 | EEO audits                                                           |       |   X   |       |       |      |
| 3 | Payroll, Compensation and hiring                                     |       |   X   |       |       |      |
| 4 | Organization Overview                                                |       |   X   |       |       |      |

#### Legal, Risk and Compliance

|#| Topics                                                                             | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------ | :---: | :---: | :---: | :---: |:---: |
| 1 | Compliance to business conduct (including hotline complaints and code of conduct violations).|       |       |       |    |  X  |
| 2 | Code of conduct, Related party transactions and other policy reviews              |      |       |       |     |    X  | 
| 3 | Legal risk assessment updates                                                     |      |       |       |     |    X  |    
| 4 | Regulatory compliance                                                             |      |   X   |       |     |       |  
| 5 | Privacy                                                                           |      |   X   |       |     |       |  
| 6 | Committee annual assessment                                                       |      |       |       |  X  |       |
| 7 | Litigation                                                                        |      |       |       |     |   X   |
| 8 | Approval of minutes                                                               |   X  |   X   |   X   |  X  |       |
| 9 | Review of Audit committee charter                                                 |   X  |       |       |     |       | 
| 10 | Related party transactions                                                       |      |       |       |     |   X   | 
| 11 | Organization Overview                                                            |      |   X   |       |     |       |

#### System

|#| Topics                                                                 | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Information Technology System updates                                |   X   |   X   |   X   |   X   |      |
| 2 | Organization Overview                                                |       |   X   |       |       |      |


#### Security Compliance

|#| Topics                                                                 | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :----------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Annual Security Operational Risk Assessment                          |       |   X   |       |       |      |      
| 2 | Security update                                                      |   X   |   X   |   X   |   X   |   X  |
| 3 | Finance application IT General Control audits                        |   X   |   X   |   X   |   X   |      |
| 4 | Organization Overview                                                |       |   X   |       |       |      |


#### Internal Audit

|#| Topics                           | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Internal audit and global annual plan                                                     |       |       |       |   X   |      |
| 2 | Internal audit activity report and annual plan update                                     |   X   |   X   |   X   |   X   |      |
| 3 | SOX - Internal control over financial reporting assessment and deficiencies status update |   X   |   X   |   X   |   X   |      |
| 4 | Internal controls (pre-Sox)                                                               |       |   X   |       |       |      |
| 5 | Internal audit charter review                                                             |   X   |       |       |       |      |
| 6 | Fraud Risk assessment                                                                     |       |   X   |       |       |      |
| 7 | Annual assessment of internal audit                                                       |       |       |   X   |       |      |
| 8 | Organization Overview                                               |       |   X   |       |        |     |

#### External Audit

|#| Topics                                                                        | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :-------------------------------------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Global audit plan and fees/Appoint External Auditor                     |       |       |       |   X   |      |
| 2 | Year-end audit results and required communications, as applicable       |   X   |   X   |   X   |   X   |      |
| 3 | Annual assessment of audit firm, engagement team and lead audit partner |       |       |   X   |       |      |
| 4 | Independence review                                                     |       |       |       |   X   |      |
| 5 | Audit                                                                   |   X   |   X   |   X   |   X   |      |


#### Closed session 

|#| Topics                                     | FY Q1 | FY Q2 | FY Q3 | FY Q4 |As needed|
|:---:| :------------------------------------- | :---: | :---: | :---: | :---: |:---: |
| 1 | Executive Session with invitees          |       |       |       |       |   X  |
| 2 | Chief Legal Officer                      |   X   |   X   |   X   |   X   |      |
| 3 | Chief Finance Officer                    |   X   |   X   |   X   |   X   |      |
| 4 | Approvals                                |   X   |   X   |   X   |   X   |      |
| 5 | External Auditor                         |   X   |   X   |   X   |   X   |      |

<br>

### Audit Committee Meeting Deck Preparation Guidelines

**Responsibility: [Chief Financial Officer](https://about.gitlab.com/company/team/#brobins) / [Principal Accounting Officer](https://about.gitlab.com/company/team/#daleb04)**

1. All the audit committee decks should be saved in [google drive](https://drive.google.com/drive/folders/1nqK9DZC84qbV6b6rKwjt0NgpVjip1WnW).
1. Refer to GitLab [Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit#gid=519993910) and identify Audit Committee Meeting Date.
1. Copy the [Format](https://docs.google.com/presentation/d/15k15TYvTGkxZizBds1geY3lTlIq1nfU9ofwwynoY9dM/edit#slide=id.g6478e21bce_0_0) of the Audit Committee meeting deck and rename the deck as **Audit Committee Meeting Month,MM, Day**
1. | Update the agenda slide of the deck by                                                                                                                                 |
    | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | a) Referring to the [master calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) -> Audit Committee calendar tab FY22 |
    | b) [Handbook](https://about.gitlab.com/handbook/board-meetings/committees/audit/#audit-committee-agenda-planner) for Audit Committee meeting agenda items                                                      |
    | c) Audit Committee meeting [notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit)                                              |
1. Set up a meeting with Principal Accounting Officer to review and update all the agenda items.
1. Once the agenda is finalized, create slides for each agenda item and assign to respective DRI’s with a due date for completion; at least  10 days before the meeting.
1. Tag all the DRI’s on **#Audit Committee** slack channel, linking the Deck and communicating the due date for completion of the deck.
1. Follow up with all the DRI's at least a week before the due date.
1. Once respective DRI's update their slides, review the format, update slides to ensure format is consistent across all the slides.
1. Set up a call with the Principal Accounting officer on the due date of the deck/ next immediate day to review the deck. Make necessary changes based on the review.
1. Set up a call for CFO's review along with Principal Accounting Officer. Make necessary changes to the deck based on CFO's review.
1. The Sr. EBA to the CFO will upload the final deck to Boardvantage at least a week prior to the meeting. The CFO will notify the Committee members of such via email and copying relevant DRIs.  
1. Update Audit Committee [meeting notes](https://docs.google.com/document/d/1D6wpUqqx9y_AcMyJr2XmdGsaauvplkLFmgbjUh9ESX0/edit#heading=h.nu90jml2xhx2) with agenda items and DRI’s.
1. On the meeting day
    - Make note of the follow-up items, add them to the agenda under ask from the Audit Committee section in in the [AC calendar](https://docs.google.com/spreadsheets/d/1C7JOGCtVJYgyjorHNch3oQT8tLtyYL9u4s88arm6d58/edit#gid=1897678424) along with the due date.
    - Schedule a call after the day of the Audit Committee meeting with the Principal Accounting Officer to review the agenda for the next meeting.
